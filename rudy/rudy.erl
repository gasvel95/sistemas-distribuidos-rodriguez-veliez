-module(rudy).
-export([init/1,handler/1,request/1,reply/1,start/1, stop/0]).

init(Port) ->
  Opt = [list, {active, false}, {reuseaddr, true}],
  case gen_tcp:listen(Port, Opt) of
    {ok, Listen} ->
      handler(Listen),
      gen_tcp:close(Listen),
      init(Port),
      ok;
    {error, Error} ->
      io:format("Se ha producido el siguiente error: ~w~n", [Error]),
      error
  end.

handler(Listen) ->
  case gen_tcp:accept(Listen) of
    {ok, Client} ->
      spawn(fun() -> request(Client) end),
      %request(Client),
      handler(Listen);
    {error, Error} ->
      io:format("Se ha producido el siguiente error: ~w~n", [Error]),
      error
  end.

request(Client) ->
    Recv = gen_tcp:recv(Client, 0),
    case Recv of
        {ok, Str} ->
            Req = http:parse_request(Str),
            Response = reply(Req),
            gen_tcp:send(Client, Response);
        {error, Error} ->
            io:format("rudy: error: ~w~n", [Error])
    end,
    gen_tcp:close(Client).

reply({{get, URI, _}, _, _}) ->
  %timer:sleep(40),
  http:ok("TODO COOL").

start(Port) ->
    register(rudy, spawn(fun() -> init(Port) end)).
    
stop() ->
    exit(whereis(rudy), "time to die").

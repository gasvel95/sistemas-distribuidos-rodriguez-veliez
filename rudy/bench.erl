-module(bench).
-compile(export_all).

bench(0, _, _,_) ->
    finished;

bench(N, Host, Port, Times) ->
  % spawn(fun() ->  bench(N-1, Host, Port, Times) end),
  {Time,Result} = timer:tc(fun() -> run(Times, Host, Port) end),
  io:format("#~w Response time: ~w microsecs ~n",[N, Time]),
  bench(N-1, Host, Port, Times).

run(N, Host, Port) ->
  if
    N == 0 ->
      ok;
    true ->
      request(Host, Port),
      run(N-1, Host, Port)
  end.

request(Host, Port) ->
  Opt = [list, {active, false}, {reuseaddr, true}],
  {ok, Server} = gen_tcp:connect(Host, Port, Opt),
  gen_tcp:send(Server, http:get("foo")),
  Recv = gen_tcp:recv(Server, 0),
  case Recv of
    {ok, _} ->
      ok;
    {error, Error} ->
      io:format("test: error: ~w~n", [Error])
  end,
  gen_tcp:close(Server).

# Sistemas distribuidos Rodriguez-Veliez



## Integrantes

- Natalia Rodriguez
- Gastón Veliez

## ¿Qué aprendimos?

Estamos aprendiendo más de earlang, como hacer un programa multithread y también sobre la libreria de gen_tcp, por ejemplo los metodos accept y recv son metodos bloqueantes que se van a quedar esperando una conexión para luego consumir y/o devolver el socket correspondiente.

## ¿Qué es lo que nos complico o con lo que tuvimos más dificultades?

Creo que una de las cosas que más se nos complico fue la sintaxis de cuando usamos erlang o mismo tenemos que usar la consola, no por la complejidad que conlleva eso, sino por una sintaxis diferente de la que estamos acostumbrados. 

## ¿Cuántos requests por segundo podemos servir? 

**_Con Sleep:_**
- 10 requests tarda 1/2 segundo.
- 100 requests tardan 5 segundos.
- 500 requests tardan 25 segundos.

Realizando las cuentas podemos servir alrededor de 1200 requests por minuto y 20 por segundo.

**_Sin Sleep:_**
- 100 requests tarda 0.06 segundos.
- 500 requests tarda 0.29 segundos.
- 1000 requests tarda 1/2 segundo.
- 10000 requets tardan 5 segundos.

Realizando las cuentas podemos servir alrededor de 2000 request por segundo y 120000 por minuto

![benchmark con y sin spawn](rudy/bench.png)

## ¿Nuestro delay artificial es importante o desaparece dentro del overhead de parsing?

El delay artificial es importante ya que se vuelve bastante notorio cuando nosotros no tenemos el sleep. Cuando le agregamos el sleep, se va viendo cuanto más se tarda en terminar toda la ejecución, y aun más dependiendo de cuanto tiempo dura el sleep, en nuestro caso le pusimos un sleep de 100 y podemos ver que por ejemplo para 500 requests tarda casi un minuto, pero sin el mismo se realiza en un segundo.  

## ¿Qué ocurre si ejecutamos los benchmarks en varias máquinas al mismo tiempo?

Si ejecutamos dos benchmarks en dos maquinas al mismo tiempo podemos ver que varia muy poco la diferencia entre una y otra. 
Para 100 requests y sin sleep da: 
- Maquina 1: 63000 milisegundos
- Maquina 2: 62000 milisegundos

Para 500 requests y sin sleep da: 
- Maquina 1: 297000 milisegundos
- Maquina 2: 281000 milisegundos

### ¿Deberíamos crear un nuevo proceso por cada request de entrada?
Siempre que el número de solicitudes de entrada sea compatible con la capacidad de concurrencia que la CPU puede manejar.Sin embargo, en momentos de alta demanda, tener demasiados procesos compitiendo por ser procesados podría no ser eficiente, lo que podría llevar a una agotación de los recursos disponibles.

### ¿Toma tiempo crear un nuevo proceso?
No toma un tiempo significativo.

### ¿Qué ocurriría si tenemos miles de requests por minuto?
El tiempo de ejecución puede variar según la cantidad de procesos en funcionamiento. Si creamos un proceso por solicitud, existe el riesgo de crear un cuello de botella o, en el peor de los casos, interrumpir el proceso principal. Por eso, la mayoría de los servidores HTTP manejan un pool de conexiones específico, ajustado a la capacidad de procesos o hilos que pueden manejar, dependiendo del nivel de procesamiento que disponga.
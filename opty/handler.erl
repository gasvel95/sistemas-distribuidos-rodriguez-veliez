-module(handler).
-export([start/3]).

start(Client, Validator, Store) ->
  spawn_link(fun() -> init(Client, Validator, Store) end).

init(Client, Validator, Store) ->
  handler(Client, Validator, Store, [], []).

handler(Client, Validator, Store, Reads, Writes) ->
  receive
    {Reference, Entry, Value, Time} ->
      Client ! {Reference, Value},
      handler(Client, Validator, Store, [{Entry, Time} | Reads], Writes);
    {read, Reference, N} ->
      case lists:keysearch(N, 1, Writes) of
        {value, {N, _, Value}} ->
          Client ! {Reference, Value},
          handler(Client, Validator, Store, Reads, Writes);
        false ->
          Entry = store:lookup(N, Store),
          Entry ! {read, Reference, self()},
          handler(Client, Validator, Store, Reads, Writes)
      end;
    {write, N, Value} ->
      Entry = store:lookup(N, Store),
      NewEntry = [{N, Entry, Value}|Writes],
      handler(Client, Validator, Store, Reads, NewEntry);
    {commit, Reference} ->
      Validator ! {validate, Reference, Reads, Writes, Client};
    stop -> ok
end.


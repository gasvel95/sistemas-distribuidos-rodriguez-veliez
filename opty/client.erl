-module(client).
-export([read/2, write/3, commit/1, stop/1]).

read(Handler, N) ->
    Ref = make_ref(),
    Handler ! {read, Ref, N},
    receive
        {Ref, Value} ->
            Value
    end.

commit(Handler) ->
    Ref = make_ref(),
    Handler ! {commit, Ref},
    receive
        {Ref, ok} -> 
            ok;
        {Ref, stop} -> 
            stop;
        {Ref, Any} -> 
            stop
    end.

write(Handler, N, Val) ->
    Handler ! {write, N, Val}.

stop(Handler) -> 
    Handler ! stop.
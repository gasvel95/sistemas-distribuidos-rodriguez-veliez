# Preguntas
## ¿Performa? ¿Cuántas transacciones podemos hacer por segundo? ¿Cuáles son las limitaciones en el número de transacciones concurrentes y la tasa de éxito? Esto por supuesto depende del tamaño del store, cuántas operaciones de write hace cada transacción, cuanto tiempo tenemos entre las instrucciones de read y el commit final. ¿Algo más?
Teniendo en cuenta que en nuestro sistema lo que vamos variando es el store y los clientes, notamos que:
   Va disminuyendo la performance, mientras el valor del store este cercano a la cantidad de clientes concurrentes. 
La taza de errores disminuye cuando el store es muy cercano a la cantidad de clientes, mientras que aumentaria cuando 
el store es mucho más bajo que la cantidad de transacciones, ya que no puede procesarlos a todos. 
   Va a haber una limitación cuando el número de transacciones concurrentes sea muy superior al store. Esto disminuye
drasticamente la taza de exito.
![benchmark](opty/benchmark.png)

## ¿Es realista la implementación del store que tenemos?
Notamos que no es realista la implementación del store. Por lo que podemos observar, se va iterando elemento por elemento
y lo que produce esto es que cuanto más alta sea la cantidad de elementos que le pasemos al store, la performance 
disminuira. Y otra cosa que pudimos observar es que estaría fallando el store cuando le pasamos un número extremadamente
alto. 
Otra cosa que podemos mencionar es que en el codigo se aborta la transacción cuando algo no fue lo esperado, cuando
lo ideal sería la utilización de deadlocks para que peleen por el recurso.

## Independientemente del handler de transacciones, ¿qué rápido podemos operar sobre el store?
Es muy rapido para pequeños accesos, pero cuando la cantidad de accesos aumenta disminuiría la velocidad del store.
En este último caso, lo que tarda más es la creación del store, que la ejecución del mismo. 

## ¿Qué sucede si hacemos esto en una red de Erlang distribuida, qué es lo que se copia cuando el handler de 
## transacciones arranca? ¿Dónde corre el handler? ¿Cuáles son los pros y contras de la estrategia de implementación?
El handler lo que terminaría copiando cuando arranca son los PIDs de referencia, y no el store entero.
El validador se convierte en un obstáculo porque funciona como un único punto de validación para todos los commits de los usuarios. A medida que aumenta el número de clientes que realizan commits, el tiempo de respuesta se ve significativamente afectado.
El handler opera en el lado del cliente y recibe los PID del servidor y del validador como parámetros. Si se desea distribuir la carga de trabajo de manera más equitativa, sería necesario crear múltiples servidores con sus respectivos validadores. Estos servidores probablemente deberían contar con una copia del almacenamiento de datos para optimizar su funcionamiento.
El pro que observamos es la simpleza de la implementación, ya que es una estructura bien sencilla de una base de datos.
Cuanto más nos acerquemos a una aproximación de una base de datos real, el sistema se complejizaría. Otra ventaja es que el proceso del handler se cierra automáticamente si el cliente se desconecta. Sin embargo, una de las mayores desventajas radica en que el servidor carece de control sobre el handler, lo que podría resultar en la apertura de un número excesivo de instancias de handler, o incluso permitir que un solo cliente abra tantos handlers como desee, sin restricciones.

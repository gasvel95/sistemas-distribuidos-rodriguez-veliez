-module(bench).
-export([startServer/1, startClient/2, runClients/2, startBench/2]).
-import(server, [start/1]).
-import(client, [read/2, write/3, commit/2, stop/0]).

startBench(M, N) ->
    startServer(M),
    {AvgResTime, StopCount, OkCount} = runClients(M, N),
    server:stop(server),
    io:format("No Oks: ~w~nOks: ~w~nAvgTime: ~wms~n", 
        [StopCount, OkCount,AvgResTime]),
    stop_bench.

startServer(N) -> 
    register(server, server:start(N)).
    
startClient(ResultsCollector, M) ->
    StartTime = erlang:system_time(milli_seconds),
    Result = clientLogic(M),
    EndTime = erlang:system_time(milli_seconds),
    ResultsCollector ! { self(), Result, (EndTime - StartTime) }.

clientLogic(M) ->
    Handler = server:open(server),
    Algorithm = exs64,
    rand:seed(Algorithm),
    Random1 = rand:uniform(M),
    Random2 = rand:uniform(M),
    client:read(Handler, Random1),
    client:write(Handler, Random1, Random2),
    client:write(Handler, Random2, Random1),
    client:read(Handler, Random1),
    client:read(Handler, Random2),
    Status = client:commit(Handler),
    Status.
    
runClients(M, N) -> 
    ResultsCollector = self(),
    [ spawn(fun() -> startClient(ResultsCollector, M) end) || _ <- lists:seq(1, N)],
    Results = [ collectRes() || _ <- lists:seq(1, N)],
    { averageTime(Results) , count(stop, Results), count(ok, Results) }. %% calcular tiempo promedio
    
collectRes() ->
    receive
        { PID, Status, Res } -> { PID, Status, Res } 
    end.
    
count(Status, Results) -> 
    Condition = fun({PID,StatusList,Res}) -> 
        StatusList == Status
    end,
    length([X || X <- Results, Condition(X)]).

averageTime(Results) -> 
    lists:sum([X || {_ , _ , X} <- Results])/length(Results).

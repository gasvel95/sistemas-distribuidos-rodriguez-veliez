-module(totyTotalOrder).

-export([start/2, stop/0, stop/1]).

start( Jitter, Sleep) ->
  L1 = multicastTotalOrder:start(Jitter),
  L2 = multicastTotalOrder:start(Jitter),
  L3 = multicastTotalOrder:start(Jitter),
  L4 = multicastTotalOrder:start(Jitter),
  L1 ! {peers, [L2, L3, L4]},
  L2 ! {peers, [L1, L3, L4]},
  L3 ! {peers, [L1, L2, L4]},
  L4 ! {peers, [L1, L2, L3]},
  register(w1, worker:start("PepeGrillo", L1, 34, Sleep)),
  register(w2, worker:start("Pinorcho", L2, 37, Sleep)),
  register(w3, worker:start("Pikarchu", L3, 43, Sleep)),
  register(w4, worker:start("Sarasa", L4, 72, Sleep)),
  timer:sleep(30000),
  stop(),
  ok.

stop() ->
  stop(w1), stop(w2), stop(w3), stop(w4).

stop(Name) ->
  case whereis(Name) of
    undefined ->
      ok;
    Pid ->
      Pid ! stop
  end.

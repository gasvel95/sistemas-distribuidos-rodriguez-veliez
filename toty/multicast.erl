-module(multicast).

-export([start/1]).

start(Jitter) ->
    spawn(fun() -> init(Jitter) end).

init(Jitter) ->
    receive
        {peers, Nodes} -> 
            open(Jitter, Nodes);
        stop ->
            ok
    end.

open(Jitter, Nodes) ->
    receive
        {start, Master} ->
            Next = 0,
            server(Master, Next, Nodes, Jitter);
        stop ->
            ok
    end.

server(Master, Next, Nodes, Jitter) ->
  receive
      {send, Msg} ->
          timer:sleep(Jitter),
          Ref = make_ref(),
          deliver(self(),Nodes,Msg),
          server(Master, Next, Nodes, Jitter)
  end.

deliver(Master, Agreed, Msg) ->
  lists:foreach(fun(Node) ->
      Node ! {response, Msg}
  end, Agreed).


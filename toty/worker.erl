-module(worker).
-export([start/4]).

-define(deadlock, 4000).

start(Name, Multicast, Seed, Sleep) ->
    spawn(fun() -> init(Name, Multicast, Seed, Sleep) end).
    
init(Name, Multicast, Seed, Sleep) ->
    Multicast ! {start, self()},
    Gui = spawn(gui, init, [Name]),
    random:seed(Seed, Seed, Seed),
    Color = { 0, 0, 0 },
    worker(Multicast , Sleep, Gui, Color),
    Gui ! stop.

worker(Multicast, Sleep, Gui, Color) ->
    Wait = random:uniform(Sleep),
    receive
        stop ->
            stop
    after Wait ->
        RandNum = random:uniform(20),
        Multicast ! {send, RandNum},
        NColor = draw(RandNum, Gui, Color),
        worker(Multicast, Sleep, Gui, NColor)
    end.

draw(Number, Gui, Color) ->
    receive
        {response, Number} -> 
            NewColor = colorChange(Color, Number),
            Gui ! {draw, NewColor},
            io:format("worker Color: ~w ~n",[NewColor]),
            NewColor;
        {response, M} -> 
            NewColor = colorChange(Color, M),
            Gui ! {draw, NewColor},
            io:format("worker Color: ~w ~n",[NewColor]),
            draw(Number, Gui, NewColor)
    end.

colorChange({R, G, B}, N) -> 
    {G, B, (R+N) rem 256}.
-module(toty).

-export([start/2, stop/0]).

start( Jitter, Sleep) ->
  L1 = multicast:start(Jitter),
  L2 = multicast:start(Jitter),
  L3 = multicast:start(Jitter),
  L4 = multicast:start(Jitter),
  L1 ! {peers, [L2, L3, L4]},
  L2 ! {peers, [L1, L3, L4]},
  L3 ! {peers, [L1, L2, L4]},
  L4 ! {peers, [L1, L2, L3]},
  register(w1, worker:start("John", L1, Sleep)),
  register(w2, worker:start("Ringo", L2, Sleep)),
  register(w3, worker:start("Paul", L3, Sleep)),
  register(w4, worker:start("George", L4, Sleep)),
  timer:sleep(30000),
  stop(),
  ok.

stop() ->
  stop(w1), stop(w2), stop(w3), stop(w4).

stop(Name) ->
  case whereis(Name) of
    undefined ->
      ok;
    Pid ->
      Pid ! stop
  end.

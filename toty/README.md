# Multicast Básico
## Analizar que pasa si tenemos un sistema que se basa en una entrega con orden total pero esto no fue claramente establecido. ¿Si la congestión es baja y no tenemos retrasos en la red, cuánto tiempo tarda antes de que los mensajes se entreguen fuera de orden?

Si tenemos un sistema basado en la entrega con orden total, pero esto no está claramente establecido, es crucial analizar el impacto en la entrega de mensajes. En condiciones de baja congestión y sin retrasos en la red, los mensajes deberían entregarse en el orden correcto. Sin embargo, cualquier incremento en el volumen de mensajes puede afectar este orden.

En el libro se menciona que la pérdida de mensajes y la inconsistencia en el ordenamiento pueden depender del método de replicación y de la necesidad de mantener actualizadas todas las réplicas. El ancho de banda consumido es proporcional al número de mensajes enviados en cada operación de entrada y salida. Por lo tanto, si los Workers envían un gran número de mensajes, el sistema de multicast deberá aumentar su capacidad para almacenar y distribuir los datos. El tiempo necesario para estimar la entrega de mensajes también dependerá de la frecuencia con la que cada Worker envía sus mensajes. Si el tiempo entre envíos es corto, el sistema puede congestionarse y enviar mensajes fuera de orden.

## Cuán difícil es hacer debug del sistema y darnos cuenta que es lo que está mal?

Inicialmente, para identificar y validar los problemas en nuestras pruebas, utilizamos una pequeña cantidad de Workers, lo que simplificó considerablemente la detección de fallos. Además, imprimimos en pantalla la identidad de cada Worker cuando este actualizaba su vista, lo que nos permitió seguir de manera secuencial los diferentes eventos.

Finalmente, utilizamos las variables de Sleep y Jitter para comprobar el funcionamiento completo del sistema. Dado que estos factores afectan significativamente el rendimiento, observamos que una configuración con valores muy pequeños para Sleep y Jitter aumentaba considerablemente la probabilidad de desincronización.

# Multicast con Orden total
## Probar usando el multicaster de orden total. ¿Mantiene los workers sincronizados? 

![benchmark](toty/testToty.png)

Observamos que puede suceder que un mensaje enviado por un worker comience a recibir confirmaciones antes que otro, aunque el segundo mensaje sea entregado primero si obtiene el acuerdo antes. Esto puede resultar en que varias ventanas se pinten del color propuesto por uno de los procesos de multicast, mientras que otras ventanas se coloreen según el otro proceso.
Hemos notado que esta implementación funciona en la mayoría de los casos. Sin embargo, también identificamos algunos casos en los que no funciona correctamente. La falla más común que encontramos ocurrió cuando reducimos los tiempos de Sleep y Jitter, que son las variables que controlan las demoras en el envío y entrega de los mensajes. 

## Tenemos muchos mensajes en el sistema, ¿cuántos mensajespodemos hacer multicast por segundo y cómo depende esto del número de workers?

Basando nuestras conclusiones en la experiencia obtenida a partir de numerosas pruebas realizadas anteriormente podemos afirmar que la cantidad de mensajes por segundo es bastante alta y no está significativamente limitada por la cantidad de Workers. Cada Worker envía su mensaje a su propio proceso de Multicast, el cual inmediatamente solicita una propuesta a sus pares. Debido a que el consenso se logra de manera distribuida, ningún proceso de Multicast experimenta sobrecarga de datos, lo que permite recibir otro mensaje de su Worker sin retrasos.

Sin embargo, dado que cada Worker tiene asignado un único proceso de Multicast, el consenso se realiza en un orden de N, donde N es la cantidad de procesos de Multicasting que deben alcanzar un acuerdo, es decir, la cantidad de Workers. Esto sugiere que existe un ligero incremento en el tiempo de consenso a medida que aumenta el número de Workers.

## Construir una red distribuida. ¿Cuán grande puede ser antes de que empiece a fallar?

Existen varios factores que pueden causar fallas en una red distribuida. Por ejemplo, un proceso puede fallar, la ejecución puede arrojar un resultado incorrecto debido a un bloqueo de procesos o interbloqueo, o puede haber un tiempo de espera expirado. Sin embargo, una gran ventaja de los sistemas distribuidos sobre los sistemas centralizados es su tolerancia a fallos. Esto no significa que nunca fallarán, pero si una tarea falla, o en nuestro caso, si un worker no recibe o no envía su mensaje, los demás workers pueden seguir funcionando correctamente.

Otra característica de una red distribuida es su capacidad de crecimiento incremental. Esto significa que se puede añadir más capacidad de procesamiento al sistema de manera gradual según las necesidades, permitiendo que el sistema sea tan grande como sea necesario. Es fundamental que el sistema funcione correctamente, es decir, que las fallas no sean causadas por errores lógicos o de implementación, sino que puedan ocurrir por razones como la pérdida de mensajes o la saturación del tráfico, entre otras.
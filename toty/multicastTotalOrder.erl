-module(multicastTotalOrder).
-export([start/1]).

start(Jitter) ->
    spawn(fun() -> init(Jitter) end).

init(Jitter) ->
    receive
        {peers, Nodes} -> 
            open(Jitter, Nodes);
        stop ->
            ok
    end.

open(Jitter, Nodes) ->
    receive
        {start, Master} ->
            Next = 0,
            Cast = [],
            Queue = [],
            server(Master, Next, Nodes, Cast, Queue, Jitter);
        stop ->
            ok
    end.

server(Master, Next, Nodes, Cast, Queue, Jitter) ->
    receive
        {send, Msg} ->
            Ref = make_ref(),
            request(self(), Nodes, Ref, Msg),
            Cast2 = cast(Cast, Ref, Nodes),
            server(Master, Next, Nodes, Cast2, Queue, Jitter);
        {request, From, Ref, Msg} ->
            From ! {proposal, Ref, Next},
            Queue2 = insert(Queue, Ref, Msg, Next),
            Next2 = increment(Next),
            server(Master, Next2, Nodes, Cast, Queue2, Jitter);
        {proposal, Ref, Proposal} ->
            case proposal(Cast, Ref, Proposal) of
                {agreed, Seq, Cast2} ->
                    agree(Nodes, Ref, Seq),
                    server(Master, Next, Nodes, Cast2, Queue, Jitter);
                Cast2 ->
                    server(Master, Next, Nodes, Cast2, Queue, Jitter)
            end;
        {agreed, Ref, Seq} ->
            timer:sleep(Jitter),
            MaxSeq = lists:max(Seq),
            Updated = update(Queue, Ref, MaxSeq),
            {Agreed, Queue2} = agreed(MaxSeq, Updated),
            Next2 = increment(MaxSeq, Next),
            deliver(Master, Agreed),
            server(Master, Next2, Nodes, Cast, Queue2, Jitter)
    end.

%Enviar el consenso a el resto de nodos
agree(Nodes, Ref, Seq) ->
    lists:foreach(fun(Node) ->
        Node ! {agreed, Ref, Seq}
    end, [self() | Nodes]).

%Si la propuesta es la ultima que esperamos , actualizar cast2 y devuelve agreed, Seq y Cast2. Si no, devuelve Cast2
proposal(Cast, Ref, Proposal) ->
    case lists:keyfind(Ref, 1, Cast) of
        {Ref, 1, Seq} -> 
            Cast2 = lists:keydelete(Ref, 1, Cast),
            {agreed, [Proposal | Seq], Cast2};
        {Ref, N, Seq} -> 
            lists:keyreplace(Ref, 1, Cast, {Ref, N-1, [Proposal | Seq]});
        false -> 
            io:format("multicast: NOT FOUND~n", [])
    end.
    
%Encolar el mensaje en la queue utilizando el numero de secuencia como clave
insert(Queue, Ref, Msg, Next) ->
    lists:keysort(3, [{proposal, Ref, Next, Msg} | Queue]).

%Incrementar numero propuesto
increment(N) ->
    N + 1.

increment(M, N) ->
    increment(max(M, N)).

%Enviar request al resto de los nodos con referencia unica
request(From, Nodes, Ref, Msg) ->
    lists:foreach(fun(Node) ->
        Node ! {request, From, Ref, Msg}
    end, [From | Nodes]).

%Actualizar la lista Cast con la referencia de request y la cantidad de nodos que tienen que contestar
cast(Cast, Ref, Nodes) ->
    [{Ref, length(Nodes) +1, []} | Cast].

%La función agreed/2 debe remover los mensajes que pueden ser entregados y retornarlos en una lista
agreed(_, Queue) ->
    lists:splitwith(fun(Elem) ->
        case Elem of
            {agreed, _, _, _} -> true;
            _ -> false
    end
    end, Queue).
    
%Actualizar la cola con el mensaje de consenso, moviendolo al final
update(Queue, Ref, Max) ->
    case lists:keyfind(Ref, 2, Queue) of
        {_, Ref, _, Msg} -> 
            lists:keysort(3, lists:keyreplace(Ref, 2, Queue, {agreed, Ref, Max, Msg}));
        false -> 
            io:format("multicast:NOT FOUND~n",[])
    end.

%Recibe y entrega los mensajes filtrados por agreed
deliver(Master,Agreed) ->
    lists:foreach(fun({ _, _, _, Msg}) ->
        io:format("multicast: ~w ~n",[Msg]),
        Master ! {response, Msg}
    end, Agreed).
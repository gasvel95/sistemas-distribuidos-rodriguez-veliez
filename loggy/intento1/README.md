## ¿Cómo sabemos que fueron impresos fuera de orden? 
## Experimentemos con el jitter y veamos si podemos incrementar o decrementar (¿eliminar?) el numero de entradas incorrectas.

Jitter es un delay que puede simular la red. Cuando hablamos de fuera de orden se habla de que se esta imprimiendo
antes el receiving que el sending. Mientras más sleep, menos cantidad de entradas veremos logueadas.

**Casos de prueba: **

- Jitter < Sleep

_Sleep: 2000 y Jitter: 0_
<br>
![jitter](/loggy/intento1/jitter0.png)

_Sleep: 1500 y Jitter: 50_
<br>
![jitter](/loggy/intento1/jitter50.png)

_Sleep: 1500 y Jitter: 250_
<br>
![jitter](/loggy/intento1/jitter250.png)

_Sleep: 3000 y Jitter: 2000_
<br>
![jitter](/loggy/intento1/jitter2000sleep3000.png)

- Jitter == Sleep
<br>
![jitter](/loggy/intento1/igual.png)


- Jitter > Sleep

_Sleep: 2000 y Jitter: 3000_
<br>
![jitter](/loggy/intento1/mayorJitter2.png)

_Sleep: 1000 y Jitter: 3000_
<br>
![jitter](/loggy/intento1/mayorJitter3.png)

Después de varias pruebas realizadas llegamos a las siguientes conclusiones: 

1. El Jitter debe de ser 0 para que los mensajes se envien en orden. Ya cuando se cambia ese valor, por más que el jitter sea muy bajo se manda fuera de orden
2. Cuando el sleep es menor o igual al jitter estamos notando que se van perdiendo mensajes, es decir que no llegamos a loguearlo.
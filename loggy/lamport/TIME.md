## Reporte modulo Time
Las primeras tres funciones del módulo de tiempo son bastante estándar. Inicializamos un nuevo worker.
con el tiempo de Lamport puesto a cero (función **zero**). La función **inc** incrementa un valor de uno
con cada nuevo evento que es enviado o recibido por el worker. Para recibir un mensaje también
realizar una fusión que garantice que la marca de tiempo de Lamport del trabajador se actualice para utilizar el
"max" de los dos valores antes de realizar un incremento en este valor.
La mayor parte del pedido se realiza en el resto de las funciones que se muestran a continuación.

La función **clock** toma una lista de nodos e inicializa cada nodo a cero. este es nuestro comienzo para realizar el pedido.

La función **update** toma un nodo, un Time y la lista de nodos mantenida en Clock. Busca el nodo utilizando la función keyreplace y, si lo encuentra, lo actualiza con su nueva marca de tiempo Lamport.

La función **safe** se utiliza para comprobar si es seguro imprimir un mensaje.
Hacemos esto de una manera fácil ordenando primero nuestra lista de Nodos (Clock) y colocándola en la variable UpdatedClock. Luego obtenemos el "Time" de la primera entrada de UpdatedClock y lo comparamos con el Time que estamos pasando a la función. Esta función es más fácil de comprender mirándolo junto con la función checkSafe en el modulo loggy.

Con esto implementado vemos una impresión de logs de manera correcta con tiempo Lamport:
![Lamport Ok](loggy/lamport/Lamport1000500.png) 

Sin embargo, si el jitter es igual o mayor al tiempo de sleep vamos a seguir "perdiendo" mensajes:
![Lamport No Ok](loggy/lamport/Lamport10001000.png)

## ¿Qué es lo que el log final nos muestra? ¿Los eventos ocurrieron en el mismo orden en que son presentados en el log?¿Qué tan larga será la cola de retención?

Como lo tenemos implementado, el log final nos muestra cada evento con su timestamp asociado, la cantidad de elementos que quedaron en la cola de mensajes y el contenido de esa cola al finalizar. Esto nos permite detectar si hay algún error de orden y que es lo que estamos reteniendo en la cola de mensajes aún no seguros.
En cuanto al tamaño de la cola de retención, observamos que cuanto menor es el jitter, mayor es la probabilidad de encolar algún mensaje que no esté seguro de imprimirse. Por ejemplo, al tener un jitter de medio segundo, descubrimos que la cola tenía, como máximo, 10 elementos.
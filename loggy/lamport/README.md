## Hagamos algunos tests e identifiquemos situaciones donde las entradas de log sean impresas en orden incorrecto. 
## ¿Cómo identificamos mensajes que estén en orden incorrecto? 

Notamos que cada worker está controlando una marca de tiempo ahora, pero el logger todavía puede imprimir los eventos muy temprano. A veces, esto puede causar que los mensajes estén en el orden correcto en la marca de tiempo general de Lamport pero en el orden incorrecto en una marca de tiempo individual. Logramos reconocer esto al notar que algunos mensajes de **sending** aparecían después de los mensajes **received**, aunque estuvieran correctamente ordenados según sus marcas de tiempo de Lamport.

## ¿Qué es siempre verdadero y qué es a veces verdadero? ¿Cómo lo hacemos seguro?

Siempre es garantizado que los mensajes de un worker específico estarán organizados según su orden para ese worker en particular. A veces, y dependiendo del jitter, nos encontramos con algunos mensajes con el orden correcto.
Algo que pensamos es aguardar por un mensaje **received** antes de buscar uno **sending**, y luego ordenar los logs cronológicamente. No obstante, no podemos garantizar que todos los workers hayan transmitido rápidamente los logs, lo que podría causar una impresión desordenada. Esta problemática será solventada mediante la solución propuesta más adelante.

Una desventaja de utilizar el tiempo de Lamport es que si un proceso tiene una marca de tiempo más alta que otro, aún no podemos estar seguros de si ese evento ocurrió antes o después del otro evento al que se compara.
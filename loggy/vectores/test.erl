-module(test).
-export([run/2]).

run(Sleep, Jitter) ->
  AllNodes = [john, paul, ringo, george],
  Log = loggy:start(AllNodes),
  A = worker:start(john, Log, 13, Sleep, Jitter, AllNodes),
  B = worker:start(paul, Log, 23, Sleep, Jitter, AllNodes),
  C = worker:start(ringo, Log, 36, Sleep, Jitter, AllNodes),
  D = worker:start(george, Log, 49, Sleep, Jitter, AllNodes),
  worker:peers(A, [B, C, D]),
  worker:peers(B, [A, C, D]),
  worker:peers(C, [A, B, D]),
  worker:peers(D, [A, B, C]),
  timer:sleep(5000),
  loggy:stop(Log),
  worker:stop(A),
  worker:stop(B),
  worker:stop(C),
  worker:stop(D).

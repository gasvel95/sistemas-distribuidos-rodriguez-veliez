-module(loggy).
-export([start/1, stop/1]).

start(Nodes) ->
    spawn_link(fun() ->init(Nodes) end).

stop(Logger) ->
    Logger ! stop.

init(Nodes) ->
    Queue = [],
    Clock = vect:clock(Nodes),
    loop(Clock, Queue).

loop(Clock, Queue) ->
    receive
        {log, From, Time, Msg} ->
            UpdatedClock = vect:update(From, Time, Clock),
            UpdatedQueue = [{From, Time, Msg} | Queue],
            SortedQueue = lists:keysort(2, UpdatedQueue), 
            NewQueue = checkSafe(UpdatedClock, SortedQueue),
            loop(UpdatedClock, NewQueue);
        stop ->
            io:format("\nQueue ~p~nSize of Queue: ~w~nClock ~p~n", [Queue, length(Queue), Clock]),
            ok
    end.
    
log(From, Time, Msg) ->
    io:format("log: ~w ~w ~p~n", [Time, From, Msg]).

checkSafe(Clock, Queue) ->
    if Queue == [] ->
        [];
    true ->
        [{From, Time, Msg} | Rest] = Queue,
        case vect:safe(Time, Clock) of
            true -> 
                log(From, Time, Msg),
                checkSafe(Clock, Rest);
            false -> 
                Queue
        end
    end.
## ¿Sería posible usar la cola de mensajes Erlang y dejar que los mensajes se encolen hasta que se libere el lock? 

Si sería posible la utilización de las colas de mensajes de Erlang en el método held ubicado en el lock, quedandonos solo con los mensajes "Release" cuando el nodo termine con lo que debe de hacer para poder recibirlos. En la lista de waiting que se tiene se estaría perdiendo, ya que al llamarse el open se reinia el nodo. 

## La razón por la que lo implementamos de esta manera es para hacer explícito que los mensajes son tratados aún en el estado held. ¿Por qué no estamos esperando mensajes ok?

No estamos esperando mensajes "OK" debido a que el worker se encuentra procesando en ese momento y el mismo se encuentra con el locker, por lo que no esta enviando peticiones "request". 

## Seremos fácilmente capaces de testear diferentes locks con diferentes parámetros Sleep y Work. ¿Funciona bien? ¿Qué ocurre cuando incrementamos el riesgo de un conflicto de lock? ¿Por qué?

Cuando incrementamos el worker nos encontramos incrementando los riesgos de conflictos con los locks, de esta manera los locks se liberan más rapidamente dejando que los demás sigan con su trabajo. Y otra cosa que pudimos observar es que como una consecuencia de tener mucho menos tiempo los locks, se incrementa el número de deadlocks. 

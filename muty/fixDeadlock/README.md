## Implementemos esta solución en un módulo llamado lock2, y veamos que funciona aunque tengamos alta contención. ¿Funciona? Hay una situación que debemos tratar correctamente. Si no, corremos el riesgo de tener dos procesos en la sección crítica al mismo tiempo. 
Por las pruebas este algoritmo funciona, sin embargo, presenta una desventaja considerable. Resulta que un lock de alta prioridad tendría una mayor posibilidad de acceder a la sección crítica, mientras que los de prioridad más baja tendrían menos oportunidades. Este escenario podría desembocar en una situación de inanición (nunca ejecutar en la sección crítica) para los locks de baja prioridad.

## ¿Podemos garantizar que tenemos un solo proceso en la sección crítica en todo momento? 
 Si algún proceso está dentro de la sección crítica, no va a dar el visto bueno a ningún otro. Por lo tanto creemos que si.

## ¿Qué tan eficientemente lo hace y cuál es la desventaja? Reportar cuáles son los resultados.
No hemos observado cambios significativos en la eficiencia en comparación con la implementación del lock1. Suponemos que con un gran número de locks, la competencia por la sección crítica debería concluir mucho más rápido ,ya que los primeros Ids tendrán una prioridad mayor que el resto y accederán a la sección crítica con mayor rapidez.
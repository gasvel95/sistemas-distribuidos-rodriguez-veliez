## ¿Puede darse la situación en que un worker no se le da prioridad al lock a pesar de que envió el request a su lock con un tiempo lógico anterior al worker que lo consiguió?
Podría ser el caso, ya que otro lock con un tiempo mayor pudo haber obtenido la sección crítica primero, mientras que el anterior no logró consultar a todos los necesarios a tiempo. La eficiencia que logramos fue similar a la anterior.

## Finalmente escribir un reporte que especifique cómo se resolvieron lock2 y lock3. Describir también que problemas y conclusiones fueron encontrados. Además se debe informar que pros y contras se ven en cada una de las soluciones.
### Implementación
Lock 2:
En esta segunda implementación, se introduce el uso del Id del lock recibido como parámetro en su construcción.
El Id juega un papel clave en el estado de espera ("waiting"), donde se compara con el Id recibido en una solicitud. Si el Id recibido tiene más prioridad que el nuestro, concedemos el permiso al proceso solicitante y enviamos una nueva solicitud a ese mismo proceso. Esta acción previene posibles ingresos múltiples a la sección crítica si ambos procesos responden simultáneamente. Por otro lado, si nuestro Id es más prioritario o igual al recibido, hacemos esperar al proceso solicitante, evitando así ingresos concurrentes a la sección crítica.

Lock 3:
En esta tercera implementación, cada lock cuenta con un reloj de Lamport. Al enviar una solicitud, el reloj del lock se incrementa, y al recibir solicitudes, se actualiza el reloj. El reloj más alto se conserva en este caso.
Al recibir una solicitud, el lock debe determinar si su tiempo es menor que el del otro. Si es así, hace esperar al proceso solicitante. En caso contrario, si el tiempo del otro es menor, se concede el permiso. Si los tiempos son iguales, se desempata por el Id.
La forma de comprobar y elegir el lock es muy similar al Lock 2, lo unico que cambia es que en lugar de comparar prioridades, se comparan tiempos. Además hay una logica extra de ir aumentando el reloj interno. Para las comparaciones y funciones complementarias volvemos a utilizar el modulo time del tp anterior.

### Pruebas y Experiencia:
Realizamos varias pruebas, y la interfaz gráfica de usuario (GUI) nos facilitó mucho la verificación de si un lock funcionaba correctamente o no (cuando dos workers tenían el color rojo, concluíamos que había un problema con el lock). Sin embargo, también enfrentamos dificultades para depurar cuando algún lock no funcionaba correctamente.

### Problemas:
En el lock 2, nos encontramos con un problema: en el estado de espera, solo enviábamos la confirmación ("ok") al proceso con el ID prioritario que envió la solicitud, lo que provocaba que varios workers pudieran ingresar a la zona crítica al mismo tiempo.
Recordamos lo discutido en clase y comprendimos que, además de enviar la confirmación al proceso con mayor prioridad, también debíamos enviarle una nueva solicitud. Al agregar estas líneas de código, pudimos resolver el problema de múltiples workers ingresando a la sección crítica simultáneamente.

### Conclusión:
Las implementaciones en general resultaron similares. Aprendimos sobre sobre un algoritmo de lock distribuido.Descubrimos que pueden ser algoritmos distribuidos, aunque con una complejidad adicional y algunos compromisos.
Deducimos que la mejor implementación es la última, utilizando los relojes de Lamport, ya que prioriza al proceso que primero solicita el permiso. Consideramos que la segunda implementación no es óptima porque los locks con menor prioridad pueden quedar bloqueados permanentemente, lo que se conoce como "starvation".


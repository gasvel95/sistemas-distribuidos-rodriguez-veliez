-module(lock3).

-export([start/1]).

start(Id) ->
  spawn(fun() -> init(Id) end).

init(_) ->
  receive
    {peers, Peers} ->
      Time = time:zero(),
      open(Peers, Time);
    stop ->
      ok
  end.

open(Nodes, Time) ->
  receive
    {take, Master} ->
      UpdtTime = Time + 1,
      Refs = requests(Nodes, Time),
      wait(Nodes, Master, Refs, [], Time);
    {request, From, Ref, TimeLock} ->
      From ! {ok, Ref, TimeLock},
      UpdtTime = time:merge(Time, TimeLock),
      open(Nodes, UpdtTime);
    stop ->
      ok
  end.

requests(Nodes,Time) ->
  lists:map(fun(P) ->
    R = make_ref(),
    P ! {request, self(), R, Time},
    R
            end, Nodes).


wait(Nodes, Master, [], Waiting, Time) ->
  Master ! taken,
  held(Nodes, Waiting, Time);

wait(Nodes, Master, Refs, Waiting, Time) ->
  receive
    {request, From, Ref, TimeLock} ->
      UpdtTime = time:merge(Time,TimeLock),
      case time:leq(Time,TimeLock) of
        true  ->
          wait(Nodes, Master, Refs, [{From, Ref}|Waiting],UpdtTime);
        false ->
          From ! {ok, Ref, Time},
          wait(Nodes,Master, Refs, Waiting, UpdtTime)
      end;          
    {ok, Ref, TimeLock} ->
      UpdtTime = time:merge(Time,TimeLock),
      Refs2 = lists:delete(Ref, Refs),
      wait(Nodes, Master, Refs2, Waiting, UpdtTime);
    release ->
      ok(Waiting, Time),
      open(Nodes, Time)
  end.

ok(Waiting, Time) ->
  lists:foreach(fun({F,R}) -> F ! {ok, R, Time} end, Waiting).

held(Nodes, Waiting, Time) ->
  receive
    {request, From, Ref, TimeLock} ->
      UpdtTime = time:merge(Time,TimeLock),
      held(Nodes, [{From, Ref}|Waiting], UpdtTime);
    release ->
      ok(Waiting,Time),
      open(Nodes,Time)
  end.

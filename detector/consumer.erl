-module(consumer).
-export([start/1, stop/0]).

start(Producer) ->
  Consumer = spawn(fun() -> init(Producer) end),
  register(consumer, Consumer).

stop() ->
  consumer ! stop,
  exit(whereis(consumer), "time to die").


init(Producer) ->
  Monitor = monitor(process, Producer),
  Producer ! {hello, self()},
  consumer(0, Monitor).

consumer(PreviousPing, Monitor) ->
  receive
    {ping, N} ->
      if
        N == PreviousPing ->
          io:format("El ping es: ~w~n", [N]),
          consumer(PreviousPing+1, Monitor);
        true ->
          io:format("Warning~n",[]),
          consumer(PreviousPing+1, Monitor)
      end;
      bye ->
        io:format("Bye");
      stop ->
        io:format("Stop");
    {'DOWN', Monitor, process, Object, Info} ->
      io:format("~w died; ~s~n", [Object, Info]),
      consumer(PreviousPing, Monitor);
    Any ->
      io:format("Error es: ~s~n", [Any]),
    consumer(PreviousPing, Monitor)
  end.
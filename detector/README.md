# Preguntas 
## 1. ¿Qué mensaje se da como razón cuando el nodo es terminado?¿Por que?
Se da el mensaje "noconnection",ya que el monitor detecta que el proceso ya no está corriendo y envia el mensaje DOWN.

## 2. ¿Qué sucede si matamos el nodo Erlang en el producer?
Si matamos el nodo:  ![kill Node](detector/resources/killNode.png) 

Si enviamos el mensaje crash sobre el producer: ![crash Node](detector/resources/inducedCrash.png)  

## 3. Ahora probemos desconectar el cable de red de la máquina corriendo el producer y volvamos a enchufarlo despues de unos segundos. ¿Qué pasa?
El consumer sigue funcionando esperando el mensaje y actualizandose con los pings atrasados todos al mismo tiempo. Luego de eso continua su funcionamiento normal.
![short wait disconnect](detector/resources/shortWait.png) 

## 4. Desconectemos el cable por períodos mas largos. ¿Qué pasa ahora?
Se desconectó por unos 30 segundos y el consumer quedó esperando mensajes hasta que , al conectar de nuevo el cable, lanzó el mensaje: 
![long wait disconnect](detector/resources/longWait.png) 

## 5. ¿Qué significa haber recibido un mensaje 'DOWN'? ¿Cuándo debemos confiar en el?
Esto indica que el monitor ha identificado que el proceso que está siendo supervisado ha dejado de funcionar por alguna razón, pero no garantiza que pueda restablecerse la conexión. Consideramos que el mensaje del monitor sirve únicamente como una notificación informativa, y en algún momento se debe tomar la decisión de considerar que la conexión con el otro nodo está perdida.

## 6. ¿Se recibieron mensajes fuera de orden, aun sin haber recibido un mensaje ’DOWN’?
No, no hemos podido conseguir que se hayan recibido mensajes fuera de orden.

## 7. ¿Qué dice el manual acerca de las garantías de envíos de mensajes?
Se puede suponer que los mensajes se ordenan en la cola según el momento en que llegaron a su destino, pero no garantiza que los mensajes enviados de una máquina a otra realmente lleguen a su destino. Existe la posibilidad de que los mensajes se pierdan durante la comunicación.
Según la documentación, los procesos en Erlang se comunican entre señales (en los cuales los mensajes estan incluidos), esto nos ayuda a evitar bloqueos y a la vez garantiza que las señales entre dos procesos llegan en el orden que fueron enviadas. Aunque es posible perder mensajes si hubo alguna falla en el enlace de distribución. 
-module(client).
-export([test/3,test/2, start/1, stop/0]).

start(Resolver) ->
    register(client, spawn(fun()-> init() end)),
    register(guiClient, spawn( fun() -> gui:startClient(client,Resolver) end)).

init() ->
    receive
        {resolve,Host,Resolver,Gui} ->
            test(Host,Resolver,Gui);
        stop ->
            io:format("Stopped ~w ... ", ["as"]),
            ok
    end.

stop() ->
    client ! stop.

test(Host, Resolver) ->
    io:format("Client: looking up ~w~n", [Host]),
    Resolver ! {request, self(), Host},
    receive
        {reply, {host, Pid}} ->
            io:format("Client: sending ping to host ~w ... ", [Host]),
            Pid ! {ping, self()},
            receive
                pong ->
                    io:format("Client: pong reply~n")
            after 1000 ->
                io:format("Client: no reply~n")
            end;
        {reply, unknown} ->
            io:format("Client: unknown host~n", []),
            ok;
        Strange ->
            io:format("Client: strange reply from resolver: ~w~n", [Strange]),
            ok
    after 1000 ->
        io:format("Client: no reply from resolver~n", []),
        ok
    end.
test(Host, Resolver,Gui) ->
    io:format("Client: looking up ~w~n", [Host]),
    Resolver ! {request, self(), Host},
    receive
        {reply, {host, Pid}} ->
            io:format("Client: sending ping to host ~w ... ", [Host]),
            Pid ! {ping, self()},
            receive
                pong ->
                    Gui ! pong,
                    io:format("Client: pong reply~n")
            after 1000 ->
                io:format("Client: no reply~n")
            end;
        {reply, unknown} ->
            io:format("Client: unknown host~n", []),
            ok;
        Strange ->
            io:format("Client: strange reply from resolver: ~w~n", [Strange]),
            ok
    after 1000 ->
        io:format("Client: no reply from resolver~n", []),
        ok
    end.
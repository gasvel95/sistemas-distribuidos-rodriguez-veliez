-module(gui).

-export([start/1, init/1, startCache/0, initCache/1, startClient/2,initClient/2]).

-include_lib("wx/include/wx.hrl").

start(Name) ->
  spawn(gui, init, [Name]).

startCache() ->
  spawn(gui,initCache, ["Cache"]).

startClient(Client,Resolver) ->
  spawn(gui,initClient, [Client,Resolver]).

initClient(Client,Resolver) ->
  Width = 200,
  Height = 200,
  Server = wx:new(), %Server will be the parent for the Frame
  Frame = wxFrame:new(Server, ?wxID_ANY, "Ping", [{size,{Width, Height}}]),
  URL = wxTextCtrl:new(Frame, ?wxID_ANY,[{value, ""}]),
  Button = wxButton:new(Frame, ?wxID_ANY,  [{label, "Consultar"}, {pos,{0, 64}},{size, {150, 50}}]),
  Result = wxStaticText:new(Frame,?wxID_ANY,"Ping",[{pos,{0,120}}]),
  wxButton:connect(Button, command_button_clicked, [{callback, fun
handle_click/2}, {userData, #{url => URL, client => Client, resolver => Resolver, self => self() , env => wx:get_env()}}]),
  wxFrame:show(Frame),
  wxFrame:refresh(Frame),  
  loopClient(Frame,Result,Button,URL,Client,Resolver).

  handle_click(#wx{obj = Button, userData = #{url := Url, client := Client ,resolver := Resolver, self := Self, env := Env}},_Event) ->
    wx:set_env(Env),
    Label = wxButton:getLabel(Button),
    Val = wxTextCtrl:getValue(Url),
    Prev = string:tokens(Val,"."),
    Pred = fun(K) -> list_to_atom(K) end,
    Formatted = lists:map(Pred,Prev),
    case (Label =:= "Consultar") of 
      true ->
        Client ! {resolve,Formatted,Resolver,Self},
        ok;
      false ->
        unknown
  end.

init(Name) ->
  Width = 200,
  Height = 200,
  Server = wx:new(), %Server will be the parent for the Frame
  Frame = wxFrame:new(Server, ?wxID_ANY, atom_to_list(Name) , [{size,{Width, Height}}]),
  Text = wxStaticText:new(Frame,?wxID_ANY,"Esperando"),
  wxFrame:show(Frame),
  wxFrame:setBackgroundColour(Frame, {0,255,0}),
  wxFrame:show(Text),
  wxFrame:refresh(Frame),  
  loop(Frame).

initCache(Name) ->
  Width = 200,
  Height = 200,
  Server = wx:new(), %Server will be the parent for the Frame
  Frame = wxFrame:new(Server, ?wxID_ANY, Name, [{size,{Width, Height}}]),
  Text = wxStaticText:new(Frame,?wxID_ANY,""),
  wxFrame:show(Frame),
  wxFrame:setBackgroundColour(Frame, {0,255,200}),
  wxFrame:show(Text),
  Font2 = wxFont:new(18, ?wxFONTFAMILY_MODERN, ?wxFONTSTYLE_NORMAL, ?wxFONTWEIGHT_NORMAL),
  wxTextCtrl:setFont(Text, Font2),
  wxFrame:refresh(Frame),  
  loopCache(Frame,Text,[]).

loopCache(Frame,Text,Cache) ->
  receive
    {update, NewCache} ->
      NewText = createText(NewCache),
      [_ | Rest] = NewText,
      wxStaticText:setLabel(Text,Rest),
      wxStaticText:wrap(Text,200),
      wxFrame:refresh(Frame),
      loopCache(Frame,Text,NewCache);
    stop ->
      ok;
    Error ->
      io:format("gui: strange message ~w ~n", [Error]),
      loopCache(Frame,Text,Cache)
end.

listToString([]) ->
  "";
listToString([Head | _]) ->
  atom_to_list(Head).

createText([]) ->
  "";
createText([{Name,_,_} | Rest])->
  NewText =[ unicode:characters_to_list(listToString(Name))]++ "\n"  ++ createText(Rest),
  NewText.

loop(Frame)->
  receive
    {draw, Colour} ->
      wxFrame:setBackgroundColour(Frame, Colour),
      wxFrame:refresh(Frame),
      loop(Frame);
    stop ->
      ok;
    Error ->
      io:format("gui: strange message ~w ~n", [Error]),
      loop(Frame)
  end.

loopClient(Frame,Result,Button,URL,Client,Resolver)->
  receive
    pong ->
      wxStaticText:setLabel(Result,"pong"),
      wxFrame:refresh(Frame),
      wxButton:connect(Button, command_button_clicked, [{callback, fun
handle_click/2}, {userData, #{url => URL, client => Client, resolver => Resolver, self => self() , env => wx:get_env()}}]),
      loopClient(Frame,Result,Button,URL,Client,Resolver);
    stop ->
      ok;
    Error ->
      io:format("gui: strange message ~w ~n", [Error]),
      loopClient(Frame,Result,Button,URL,Client,Resolver)
  end.

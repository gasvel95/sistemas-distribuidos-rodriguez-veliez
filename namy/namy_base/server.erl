-module(server).
-export([start/0, start/2, stop/0]).

start() ->
    register(server, spawn(fun()-> init() end)).

start(Domain, Parent) ->
    register(server, spawn(fun()-> init(Domain, Parent) end)).

stop() ->
    server ! stop,
    unregister(server).

init() ->
    io:format("Server: create root domain~n"),
    Gui =  gui:start('root'),
    server([], 0, Gui),
    Gui ! stop.

init(Domain, Parent) ->
    io:format("Server: create domain ~w at ~w~n", [Domain, Parent]),
    Parent ! {register, Domain, {domain, self()}},
    Gui =  gui:start(Domain),
    server([], 5, Gui),
    Gui ! stop.

server(Entries, TTL,Gui) ->
    Gui ! {draw ,{5,255,200}},
    receive
        {request, From, Req}->
            io:format("Server: received request to solve [~w]~n", [Req]),
            Reply = entry:lookup(Req, Entries),
            Gui ! {draw, {255,0,100}},
            From ! {reply, Reply, TTL},
            server(Entries, TTL,Gui);
        {register, Name, Entry} ->
            Updated = entry:add(Name, Entry, Entries),
            server(Updated, TTL,Gui);
        {deregister, Name} ->
            Updated = entry:remove(Name, Entries),
            server(Updated, TTL,Gui);
        {ttl, Sec} ->
            server(Entries, Sec,Gui);
        status ->
            io:format("Server: List of DNS entries: ~w~n", [Entries]),
            server(Entries, TTL,Gui);
        stop ->
            io:format("Server: closing down~n", []),
            ok;
        Error ->
            io:format("Server: reception of strange message ~w~n", [Error]),
            server(Entries, TTL,Gui)
    end.
-module(cache).
-export([new/0, lookup/2, add/4, remove/2]).

%Empty cache
new() -> 
  [].

%lookup/2 will look in the cache and return either unknown, invalid in case a old value was found or, a valid entry, {ok,Reply}. 
lookup(Name,Cache) -> 
  case lists:keyfind(Name,1,Cache) of
    false -> 
      io:format("unknown name ~n", []),
      unknown;
    {Name, Reply, Expire} -> 
        case time:valid(Expire,time:now()) of
            true -> 
                Reply;
            false -> 
                invalid
        end
  end.
      
%Add elem to cache
add(Name,Expire,Reply,Updated) ->
  lists:keystore(Name,1,Updated,{Name,Reply,Expire}).

%Remove elem to cache
remove(Name,Cache) -> 
  lists:keydelete(Name,1,Cache).
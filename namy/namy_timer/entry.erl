-module(entry).
-export([lookup/2, add/3, remove/2 ]).

%when the server receives a request it will try to look it up in its list of entries. The lookup/2 function will return unknown if not found.
lookup(Req, Entries) ->
  case lists:keyfind(Req,1,Entries) of
    false -> 
      io:format("Unknown Req ~n", []),
      unknown;
    {Req,Entry} -> 
      Entry
  end.

add(Name,Entry,Entries) -> 
  lists:keystore(Name,1,Entries,{Name,Entry}).

remove(Name,Entries) -> 
  lists:keydelete(Name,1,Entries).
# Namy
## Preguntas planteadas en el enunciado

## In the vanilla set-up the time-to-live is zero seconds. What happens if we extend this to two or four seconds. How much is traffic reduced? 
Si activamos la caché y configuramos el valor de TTL en un número mayor que 0, todas las resoluciones de nombres se almacenan en caché, lo que reduce la cantidad de tráfico de mensajes.
## Extend it to a minute and then move hosts, that is close them down and start them up registered under a new name. When is the new server found, how many nodes needed to know about the change? 
Después de apagar el host 'www' y reiniciarlo con el nombre 'ftp', el resolver utiliza la caché para consultar al servidor [unq,edu] sobre el nuevo host, lo localiza y responde. Después de la primera solicitud, la entrada completa [ftp,unq,edu] ya está almacenada en caché.

## Our cache also suffers from old entries that are never removed. Invalid entries are removed and updated but if we never search for the entry we will not remove it.
Para solventar este problema se conversó sobre el tema y surgieron 4 posibles formas de mitigarlo:
- Utilizando el modulo timer para programar la eliminación de las entradas luego de su TTL asignado.
- Utilizando un proceso de "garbage collector" que recorra la cache cada X cantidad de tiempo buscando entries invalidas.
- Creando un proceso por nuestra cuenta de Scheduler que programe para remover las entries invalidas en el TTL asignado.
- Realizando una modificación en la gestión de la caché imponiendo un tamaño máximo de capacidad. Cuando dicho tamaño es alcanzado, se verifica la entrada que cumpla con cierto criterio (Entry mas antigua por ejemplo) para elminar y dar paso a la nueva entry.
En esta implementación se optó por explorar los dos primeros metodos. 
 

## How can the cache be better organized? How would we do to reduce search time? Could we use a hash table or a tree?
Para optimizar los tiempos de busqueda en nuestra caché decidimos utilizar la estructura de Maps en Erlang. La misma nos permite acceder de manera muy eficiente a las entradas registrandolas con una clave, que en nuestro caso es el Name de cada entrada.
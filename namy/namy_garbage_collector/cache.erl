-module(cache).
-export([new/0, lookup/2, add/4, remove/2]).

%Empty cache
new() -> 
  maps:new().

%lookup/2 will look in the cache and return either unknown, invalid in case a old value was found or, a valid entry, {ok,Reply}. 
lookup(Name,Cache) -> 
  case maps:get(Name,Cache,unknown) of
    {Name, Reply, Expire} -> 
        case time:valid(Expire,time:now()) of
            true -> 
                Reply;
            false -> 
                remove(Name,Cache),
                invalid
        end;
    unknown -> 
      io:format("unknown name ~n", []),
      unknown
  end.
      
%Add elem to cache
add(Name,Expire,Reply,Updated) ->
  maps:put(Name,{Name,Reply,Expire}, Updated).

%Remove elem to cache
remove(Name,Cache) -> 
  maps:remove(Name,Cache).

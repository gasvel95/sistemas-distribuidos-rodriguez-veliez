-module(resolver).
-export([start/1, stop/0]).

start(Root) ->
    register(resolver, spawn(fun()-> init(Root) end)).

stop() ->
    resolver ! stop,
    unregister(resolver).

init(Root) ->
    Empty = cache:new(),
    Inf = time:inf(),
    Cache = cache:add([], Inf, {domain, Root}, Empty),
    Gui = gui:startCache(),
    resolver(Cache,Gui).

resolver(Cache,Gui) ->
    receive
        {request, From, Req}->
            io:format("Resolver: request from ~w to solve ~w~n", [From, Req]),
            {Reply, Updated} = resolve(Req, Cache,Gui),
            From ! {reply, Reply},
            resolver(Updated,Gui);
        status ->
            io:format("Resolver: cache content: ~w~n", [Cache]),
            resolver(Cache,Gui);
        stop ->
            io:format("Resolver: closing down~n", []),
            ok;
        Error ->
            io:format("Resolver: reception of strange message ~w~n", [Error]),
            resolver(Cache,Gui)
        after 1000 ->
            Pred = fun(K,{_,_,Expire}) -> not time:valid(Expire,time:now()) end,
            MapToDelete = maps:filter(Pred,Cache),
            NewCache = maps:without(maps:keys(MapToDelete),Cache),
            Gui ! {update, maps:values(NewCache)},
            resolver(NewCache,Gui)
    end.

resolve(Name, Cache,Gui)->
    io:format("Resolve ~w: ", [Name]),
    case cache:lookup(Name, Cache) of
        unknown ->
            io:format("unknown ~n", []),
            recursive(Name, Cache,Gui);
        invalid ->
            io:format("invalid ~n", []),
            NewCache = cache:remove(Name, Cache),
            Gui ! {update, maps:values(NewCache)},
            recursive(Name, NewCache,Gui);
        Reply ->
            io:format("found ~w~n", [Reply]),
            {Reply, Cache}
    end.

recursive([Name|Domain], Cache,Gui) ->
    io:format("Recursive ~w: ", [Domain]),
    case resolve(Domain, Cache,Gui) of
        {unknown, Updated} ->
            io:format("unknown ~n", []),
            {unknown, Updated};
        {{domain, Srv}, Updated} ->
            Srv ! {request, self(), Name},
            io:format("Resolver: sent request to solve [~w] to ~w~n", [Name, Srv]),
            receive
                {reply, unknown, _} ->
                    {unknown, Updated};
                {reply, Reply, TTL} ->
                    Expire = time:add(time:now(), TTL),
                    NewCache = cache:add([Name|Domain], Expire, Reply, Updated),
                    Gui ! {update, maps:values(NewCache)},
                    {Reply, NewCache}
            end
    end.